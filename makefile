CC = gcc
OBJS = main.o backtrack.o generate.o guesses.o legal_vals_list.o lin_prog.o list.o parser.o printboard.o saving.o sudoku_game.o undo.o
EXEC = sudoku-console
COMP_FLAGS = -ansi -O3 -Wall -Wextra -Werror -pedantic-errors
GUROBI_COMP = -I/usr/local/lib/gurobi563/include
GUROBI_LIB = -L/usr/local/lib/gurobi563/lib -lgurobi56

$ (EXEC): $(OBJS)
	$(CC) $(OBJS) $(GUROBI_LIB) -o $(EXEC) -lm
main.o: main.c parser.h backtrack.h
	$(CC) $(COMP_FLAGS) -c $*.c
backtrack.o: backtrack.c backtrack.h legal_vals_list.h sudoku_game.h
	$(CC) $(COMP_FLAGS) -c $*.c
generate.o: generate.c generate.h undo.h lin_prog.h legal_vals_list.h backtrack.h sudoku_game.h
	$(CC) $(COMP_FLAGS) -c $*.c
guesses.o: guesses.c guesses.h undo.h lin_prog.h sudoku_game.h
	$(CC) $(COMP_FLAGS) -c $*.c
legal_vals_list.o: legal_vals_list.c legal_vals_list.h
	$(CC) $(COMP_FLAGS) -c $*.c
lin_prog.o: lin_prog.c lin_prog.h sudoku_game.h
	$(CC) $(COMP_FLAGS) $(GUROBI_COMP) -c $*.c
list.o: list.c list.h
	$(CC) $(COMP_FLAGS) -c $*.c
parser.o: parser.c parser.h backtrack.h undo.h printboard.h saving.h sudoku_game.h guesses.h generate.h
	$(CC) $(COMP_FLAGS) -c $*.c
printboard.o: printboard.c printboard.h sudoku_game.h
	$(CC) $(COMP_FLAGS) -c $*.c
saving.o: saving.c saving.h sudoku_game.h
	$(CC) $(COMP_FLAGS) -c $*.c
sudoku_game.o: sudoku_game.c sudoku_game.h backtrack.h list.h
	$(CC) $(COMP_FLAGS) -c $*.c
undo.o: undo.c undo.h sudoku_game.h
	$(CC) $(COMP_FLAGS) -c $*.c
clean:
	rm -f *.o $(EXEC)
