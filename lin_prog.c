#include "lin_prog.h"
#include "gurobi_c.h"



int build_col_constraint(int *cons, int col, int val, int N) {
    int i, curr_ind, ind;
    curr_ind = 0;
    for (i = 0; i < N; ++i) {
        ind = MAPIND(i, col, val);
        if (ind == -1) {
            continue;
        }
        cons[curr_ind] = ind;
        curr_ind++;
    }
    return curr_ind;
}

int build_row_constraint(int *cons, int row, int val, int N) {
    int i, curr_ind, ind;
    curr_ind = 0;
    for (i = 0; i < N; ++i) {
        ind = MAPIND(row, i, val);
        if (ind == -1) {
            continue;
        }
        cons[curr_ind] = ind;
        curr_ind++;
    }
    return curr_ind;
}

int build_val_constraint(int *cons, int row, int col, int N) {
    int i, curr_ind, ind;
    curr_ind = 0;
    for (i = 0; i < N; ++i) {
        ind = MAPIND(row, col, i);
        if (ind == -1) {
            continue;
        }
        cons[curr_ind] = ind;
        curr_ind++;
    }
    return curr_ind;
}

int build_square_constraint(int *cons, int square, int val, int m, int n) {
    int i, curr_ind, ind, N;
    int block_start_row, block_start_col, curr_col_in_block, curr_row_in_block;
    N = m * n;
    block_start_row = (square / m) * m;
    block_start_col = (square % m) * n;
    curr_ind = 0;
    for (i = 0; i < N; ++i) {
        curr_row_in_block = block_start_row + i % m;
        curr_col_in_block = block_start_col + i / m;
        ind = MAPIND(curr_row_in_block, curr_col_in_block, val);
        if (ind == -1) {
            continue;
        }
        cons[curr_ind] = ind;
        curr_ind++;
    }
    return curr_ind;
}

int solve_full_board(sudoku *game, bool is_ILP, double **result_board) {
    GRBenv *env;
    GRBmodel *model;
    int i, j, val, optimstatus;
    int N, error, constraint_size;
    int *constraint_inds = NULL;
    double *constraint_coeffs = NULL;
    double *target_coeffs = NULL;
    char *vtype = NULL;
    double *sol = NULL;
    double *lb = NULL;
    int ret = -1;

    N = game->N;
    error = GRBloadenv(&env, "mip1.log");
    if (error) {
        printf("ERROR %d GRBloadenv(): %s\n", error, GRBgeterrormsg(env));
        return -1;
    }

    error = GRBsetintparam(env, GRB_INT_PAR_LOGTOCONSOLE, 0);
    if (error) {
        printf("ERROR %d GRBsetintparam(): %s\n", error, GRBgeterrormsg(env));
        GRBfreeenv(env);
        return -1;
    }

    target_coeffs = (double *) calloc((size_t) N * N * N, sizeof(double));
    lb = (double *) calloc((size_t) N * N * N, sizeof(double));
    constraint_coeffs = (double *) calloc((size_t) N, sizeof(double));
    vtype = (char *) calloc((size_t) N * N * N, sizeof(char));
    constraint_inds = (int *) calloc((size_t) N, sizeof(int));
    if (!constraint_inds || !target_coeffs || !vtype || !constraint_coeffs || !lb) {
        free(constraint_coeffs);
        free(constraint_inds);
        free(target_coeffs);
        free(vtype);
        GRBfreeenv(env);
        return -1;
    }
    for (i = 0; i < N * N * N; ++i) {
        target_coeffs[i] = (double) (is_ILP ? 0 : (rand() % N));
        vtype[i] = (char) (is_ILP ? GRB_BINARY : GRB_CONTINUOUS);
        lb[i] = 0.0;
    }
    for (i = 0; i < N; ++i) {
        constraint_coeffs[i] = 1.0;
    }

    error = GRBnewmodel(env, &model, "sudoku", N * N * N, target_coeffs, is_ILP ? NULL : lb, NULL, vtype, NULL);
    if (error) {
        printf("ERROR %d GRBnewmodel(): %s\n", error, GRBgeterrormsg(env));
        ret = -1;
        goto clean;
    }
    error = GRBsetintattr(model, GRB_INT_ATTR_MODELSENSE, GRB_MAXIMIZE);
    if (error) {
        printf("ERROR %d GRBsetintattr(): %s\n", error, GRBgeterrormsg(env));
        ret = -1;
        goto clean;
    }
    for (i = 0; i < N; ++i) {
        for (j = 0; j < N; ++j) {
            val = get_square_value(game, i, j);
            if (val != 0) {
                constraint_inds[0] = MAPIND(i, j, val - 1);
                error = GRBaddconstr(model, 1, constraint_inds, constraint_coeffs, GRB_EQUAL, 1.0, NULL);
                if (error) {
                    ret = -1;
                    goto clean;
                }
            }
        }
    }

    for (i = 0; i < N; ++i) {
        for (j = 0; j < N; ++j) {
            constraint_size = build_row_constraint(constraint_inds, i, j, N);
            if (constraint_size != 0) {
                error = GRBaddconstr(model, constraint_size, constraint_inds, constraint_coeffs, GRB_EQUAL, 1.0, NULL);
                if (error) {
                    ret = -1;
                    goto clean;
                }
            }
            constraint_size = build_col_constraint(constraint_inds, i, j, N);
            if (constraint_size != 0) {
                error = GRBaddconstr(model, constraint_size, constraint_inds, constraint_coeffs, GRB_EQUAL, 1.0, NULL);
                if (error) {
                    ret = -1;
                    goto clean;
                }
            }
            constraint_size = build_val_constraint(constraint_inds, i, j, N);
            if (constraint_size != 0) {
                error = GRBaddconstr(model, constraint_size, constraint_inds, constraint_coeffs, GRB_EQUAL, 1.0, NULL);
                if (error) {
                    ret = -1;
                    goto clean;
                }
            }
            constraint_size = build_square_constraint(constraint_inds, i, j, game->m, game->n);
            if (constraint_size != 0) {
                error = GRBaddconstr(model, constraint_size, constraint_inds, constraint_coeffs, GRB_EQUAL, 1.0, NULL);
                if (error) {
                    ret = -1;
                    goto clean;
                }
            }
        }
    }
    error = GRBoptimize(model);
    if (error) {
        printf("ERROR %d GRBoptimize(): %s\n", error, GRBgeterrormsg(env));
        ret = -1;
        goto clean;
    }
    error = GRBwrite(model, "mip1.lp");
    if (error) {
        printf("ERROR %d GRBwrite(): %s\n", error, GRBgeterrormsg(env));
        ret = -1;
        goto clean;
    }
    error = GRBgetintattr(model, GRB_INT_ATTR_STATUS, &optimstatus);
    if (error) {
        printf("ERROR %d GRBgetintattr(): %s\n", error, GRBgeterrormsg(env));
        ret = -1;
        goto clean;
    }
    if (optimstatus != GRB_OPTIMAL) {
        ret = (optimstatus == GRB_INFEASIBLE) ? 1 : -1;
        goto clean;
    }
    /* get the solution - the assignment to each variable */
    sol = (double *) calloc((size_t) N * N * N, sizeof(double));
    *result_board = (double *) calloc((size_t) (N * N * N), sizeof(double));
    error = GRBgetdblattrarray(model, GRB_DBL_ATTR_X, 0, N * N * N, sol);
    if (error) {
        printf("ERROR %d GRBgetdblattrarray(): %s\n", error, GRBgeterrormsg(env));
        ret = -1;
        goto clean;
    }
    /* solution found */
    if (optimstatus == GRB_OPTIMAL) {
        for (i = 0; i < N * N * N; i++) {
            (*result_board)[i] = sol[i];
        }
        ret = 0;
        goto clean;

    } else if (optimstatus == GRB_INFEASIBLE) {
        ret = 1;
        goto clean;
    } else {
        printf("Optimization Error!\n");
        ret = -1;
        goto clean;
    }

    /* IMPORTANT !!! - Free model and environment */
    clean:
    free(constraint_coeffs);
    free(lb);
    free(constraint_inds);
    free(target_coeffs);
    free(vtype);
    free(sol);
    GRBfreemodel(model);
    GRBfreeenv(env);

    return ret;
}
