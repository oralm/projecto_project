
#ifndef PROJECTO_PROJECT_LIST_H
#define PROJECTO_PROJECT_LIST_H

#include <stdbool.h>
#include <stdlib.h>

/**
 * keeps track of a change to a single square.
 * a single move might change multiple squares.
 *
 * @param x: x value of square
 * @param x: x value of square
 * @param before: old value of square
 * @param after: new value of square
 * @param next: next change in this list.
 */
typedef struct changenode {
    int x;
    int y;
    int before;
    int after;
    struct changenode *next;
} ChangeNode;


/**
 * keeps track of a the changes caused by a single move.
 * a single move might change multiple squares.
 *
 * @param next: next change in this list.
 * @param prev: previous change in this list.
 * @param changes: a list of the changes this move made.
 */
typedef struct commandnode {
    struct commandnode *next;
    struct commandnode *prev;
    bool is_head;
    ChangeNode *changes;
} CommandNode;

/**
 * frees a list of ChangeNodes
 *
 * @param node the first node in the list
 */
void free_change_list(ChangeNode *node);

/**
 * frees the following list of CommandNodes
 *
 * @param node the first node in the list
 */
void free_command_list(CommandNode *node);

/**
 * frees the entire CommandList in both directions
 *
 * @param node any node in the list
 */
void free_full_command_list(CommandNode *node);

/**
 * creates a new, empty command and appends it to the list
 *
 * @param prev_command: old list head
 * @return the newly created command. NULL if malloc failed
 */
CommandNode *add_command(CommandNode *prev_command);

/**
 * creates a new change and appends it to the list
 *
 * @param current_command: current CommandNode
 * @param x: change x
 * @param y: change y
 * @param old_val: previous square value
 * @param new_val: new square value
 * @return success
 */
bool add_change(CommandNode *current_command, int x, int y, int old_val, int new_val);

CommandNode *create_command_list_head();

#endif /*PROJECTO_PROJECT_LIST_H*/
