#ifndef PROJECTO_PROJECT_GENERATE_H
#define PROJECTO_PROJECT_GENERATE_H

#include <stdbool.h>
#include "sudoku_game.h"

/**
 * inserts a legal value into game at the indecies
 *
 * @param game the game
 * @param col column
 * @param row row
 * @return success
 */
bool choose_legal_val(sudoku *game, int col, int row);

/**
 * solves a board with ILP and fills it.
 * @param game game
 * @return 0 on succ, 1 when no solution found, -1 on fatal error
 */
int solve_board(sudoku *game);

/**
 * generates a board. make sure x>#empty cells and Y<total cells
 *
 * @param game game
 * @param X cells to add
 * @param Y cells to leave
 * @return 0 - success, -1 - malloc error, -2 - solve error
 */
int generate(sudoku *game, int X, int Y);

/**
 * validates a board
 *
 * @param game game
 * @return 0 - solvable, -1 - malloc error, 1 - no solution.
 */
int validate(sudoku *game);

#endif /*PROJECTO_PROJECT_GENERATE_H*/
