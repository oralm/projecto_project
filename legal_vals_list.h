
#ifndef PROJECTO_PROJECT_LEGAL_VALS_LIST_H
#define PROJECTO_PROJECT_LEGAL_VALS_LIST_H

#include <stdbool.h>
/**
 * a list of legal values in a cell for use in backtracking
 *
 * @param value: a legal value
 * @param next: the next node
 */
typedef struct legalvalsnode {
    int value;
    struct legalvalsnode *next;
} LegalValsNode;

/**
 * frees a list of LegalValsNodes
 *
 * @param node the first node in the list
 */
void free_legal_vals_list(LegalValsNode *node);

/**
 * adds a legal value to a list
 *
 * @param value: new value
 * @param node: exsisting node in the list
 * @return success
 */
LegalValsNode * add_legal_value(int value, LegalValsNode *node);


#endif /*PROJECTO_PROJECT_LEGAL_VALS_LIST_H*/
