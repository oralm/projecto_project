
#include "undo.h"



bool undo(sudoku *game, bool print_changes) {
    ChangeNode *curr;
    if (game->current_command->is_head) {
        return false;
    }
    curr = game->current_command->changes;
    if (print_changes){
        printf("Values changed in undo:\n");
    }
    while (curr != NULL) {
        apply_move(game, curr, false, print_changes);
        curr = curr->next;
    }
    game->current_command = game->current_command->prev;
    update_errors(game);
    return true;
}

bool redo(sudoku *game, bool print_changes) {
    ChangeNode *curr;
    if (game->current_command->next == NULL) {
        return false;
    }
    game->current_command = game->current_command->next;
    curr = game->current_command->changes;
    if(print_changes){
        printf("Values changed in redo:\n");
    }
    while (curr != NULL) {
        apply_move(game, curr, true, print_changes);
        curr = curr->next;
    }
    update_errors(game);
    return true;
}

void apply_move(sudoku *game, ChangeNode *node, bool is_redo, bool print_changes) {
    int val;
    if (is_redo) {
        val = node->after;
    } else {
        val = node->before;
    }
    if(print_changes){
        printf("<%d,%d> from %d to %d\n", node->x + 1, node->y + 1, get_square_value(game, node->x, node->y), val);
    }
    set_square_value(game, node->x, node->y, val);
}
