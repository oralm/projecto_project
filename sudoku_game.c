#include <stdio.h>
#include "sudoku_game.h"
#include "backtrack.h"


void set_square_value(sudoku *game, int row, int col, int value) {
    game->board[row * game->N + col].value = value;
}

int get_square_value(sudoku *game, int row, int col) {
    return game->board[row * game->N + col].value;
}

void set_square_state(sudoku *game, int row, int col, State state) {
    game->board[row * game->N + col].state = state;
}

State get_square_state(sudoku *game, int row, int col) {
    return game->board[row * game->N + col].state;
}

void mark_state_as_error(sudoku *game, int row, int col) {
    if (get_square_state(game, row, col) == normal) {
        set_square_state(game, row, col, error);
    }
}

void mark_state_as_valid(sudoku *game, int row, int col) {
    if (get_square_state(game, row, col) == error) {
        set_square_state(game, row, col, normal);
    }
}

bool is_erroneous(sudoku *game, int row, int col) {
    int i, block_start_row, block_start_col, curr_row_in_block, curr_col_in_block;
    int value = get_square_value(game, row, col);
    if (value == 0) {
        return false;
    }
    block_start_row = row - (row % game->m);
    block_start_col = col - (col % game->n);
    for (i = 0; i < game->N; i++) {
        if (value == get_square_value(game, i, col) && i != row) { /* checks over whole row */
            return true;
        }
        if (value == get_square_value(game, row, i) && i != col) { /* checks over whole column */
            return true;
        }
        curr_row_in_block = block_start_row + i % game->m; /*calculates coord. and checks over whole block */
        curr_col_in_block = block_start_col + i / game->m;
        if (value == get_square_value(game, curr_row_in_block, curr_col_in_block) &&
            (curr_row_in_block != row || curr_col_in_block != col)) {
            return true;
        }
    }
    return false;
}

void update_errors(sudoku *game) {
    int row, col;
    for (row = 0; row < game->N; row++) {
        for (col = 0; col < game->N; col++) {
            if (is_erroneous(game, row, col)) {
                mark_state_as_error(game, row, col);
            } else {
                mark_state_as_valid(game, row, col);
            }
        }
    }
}


bool valid_board(sudoku *game) {
    int i, j;
    for (i = 0; i < game->N; i++) {
        for (j = 0; j < game->N; j++) {
            if (get_square_state(game, i, j) == error) {
                return false;
            }
        }
    }
    return true;
}

bool is_legal_value(sudoku *game, int row, int col, int value) {
    int prev_value = get_square_value(game, row, col);
    set_square_value(game, row, col, value);
    if (is_erroneous(game, row, col)) {
        set_square_value(game, row, col, prev_value);
        return false;
    }
    set_square_value(game, row, col, prev_value);
    return true;
}

bool new_board(sudoku *game) {
    int i, j;
    game->m = 3;
    game->n = 3;
    game->N = 9;
    game->board = (square *) calloc((size_t) (game->N * game->N), sizeof(square));
    if (game->board == NULL) {
        return false;
    }
    for (i = 0; i < 9; i++) { /* initialize all squares in board to value 0 */
        for (j = 0; j < 9; j++) {
            set_square_value(game, i, j, 0);
        }
    }
    game->current_command = create_command_list_head();
    game->mode = edit;
    game->mark_errors = 1;
    return true;
}

void free_game(sudoku *game) {
    game->m = 0;
    game->n = 0;
    game->N = 0;
    free(game->board);
    game->board = NULL;
    free_full_command_list(game->current_command);
    game->current_command = NULL;
}

int find_only_legal_value(sudoku *game, int row, int col) {
    int value, legal_value = 0;
    for (value = 1; value <= game->N; value++) {
        if (is_legal_value(game, row, col, value)) {
            if (legal_value != 0) { /* if it is not the first legal value found, return 0 */
                return 0;
            }
            legal_value = value;
        }
    }
    return legal_value;
}

bool is_board_full(sudoku *game) {
    if (get_last_empty_square(game) == -1) {
        return true;
    }
    return false;
}

void check_if_solved(sudoku *game) {
    if (game->mode == solve && is_board_full(game)) {
        update_errors(game);
        if (valid_board(game)) {
            printf("Game Solved! You win! Ding ding ding!\n");
            game->mode = init;
        } else {
            printf("Board filled unsuccessfully...\n");
        }
    }
}