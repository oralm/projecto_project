


#ifndef PROJECTO_PROJECT_PRINTBOARD_H
#define PROJECTO_PROJECT_PRINTBOARD_H

#include "sudoku_game.h"
#include <stdio.h>

/**
 * prints whole board of game according to the assignment printing format.
 * Includes all separator rows and cells data.
 * @param game
 */
void print_board(sudoku *game);

#endif /*PROJECTO_PROJECT_PRINTBOARD_H*/
