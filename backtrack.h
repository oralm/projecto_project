#include "sudoku_game.h"
#include "legal_vals_list.h"

#ifndef PROJECTO_PROJECT_BACKTRACK_H
#define PROJECTO_PROJECT_BACKTRACK_H

/**
 * returns the value of the first node in the list and frees it
 *
 * @param legal_values: legal values array
 * @param ind: index to get value for
 * @return: the value of the first node
 */
int get_next_legal_value(LegalValsNode **legal_values, int ind);

/**
 * creates a list of all legal values in a square
 *
 * @param ind: square index
 * @param game: game to check legality
 * @param legal_values: array of list heads for each square
 */
void fill_legal_vals(int ind, sudoku *game, LegalValsNode **legal_values);

/**
 * counts all legal values in the square
 *
 * @param ind: square index
 * @param game: game to check legality
 * @return num of legal values in ind in game
 */
int count_legal_vals(int ind, sudoku *game);
/**
 * returns the last empty square in the game
 *
 * @param the game
 * @return the index of the last empty square or -1 if no empty square
 */
int get_last_empty_square(sudoku *game);

/**
 * uses non-recursive backtracking to get the number of solutions for a game.
 * Make sure the board is not completely full before calling,
 * and doesn't already contain any illegal values.
 *
 * @param game: the game 
 * @return the number of solutions for game.
 */
int num_solutions(sudoku *game);

#endif /*PROJECTO_PROJECT_BACKTRACK_H*/
