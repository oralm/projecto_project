
#include "saving.h"

/**
 * Converts every non blank cell to fixed.
 * @param game
 */
void convert_states_edit_mode_to_save(sudoku *game){
    int i, j;
    for (i = 0; i < game->N; i++){
        for (j = 0; j < game->N; j++){
            if (get_square_value(game, i, j) != 0){
                set_square_state(game, i, j, fixed);
            }
        }
    }
}
bool save(sudoku *game, char *path) {
    int i, j;
    FILE *file;
    if (game->mode == edit) {
        if(!valid_board(game)){
            printf("Can't save erroneous board in edit mode!\n");
            return true;
        }
        convert_states_edit_mode_to_save(game);
    }
    file = fopen(path, "w");
    if (file == NULL) {
        printf("Error opening file!\n");
        return false;
    }
    fprintf(file, "%d %d\n", game->m, game->n);
    for (i = 0; i < game->N; ++i) {
        for (j = 0; j < game->N; j++) {
            switch (game->board[i * (game->N) + j].state) {
                case normal:
                    fprintf(file, "%d ", game->board[i * (game->N) + j].value);
                    break;
                case fixed:
                    fprintf(file, "%d. ", game->board[i * (game->N) + j].value);
                    break;
                case error:/* edit mode never gets here, since the board is validated. */
                    fprintf(file, "%d ", game->board[i * (game->N) + j].value);
                    break;
            }
        }
        /* remove trailing space
         * */
        fseek(file, SEEK_CUR, -1);
        fprintf(file, "\n");
    }
    fclose(file);
    return true;
}

bool load(sudoku *game, char *path, Mode mode){
    int i, j;
    char fixed_c;
    int res;
    square *board;
    FILE *file;
    file = fopen(path, "r");
    if (file == NULL) {
        printf("Error opening file!\n");
        return false;
    }
    game->mode = mode;
    game->current_command = create_command_list_head();
    res = fscanf(file, " "); /* clears whitespace */
    if(res < 0){
        printf("scan error!\n");
        fclose(file);
        return false;
    }
    res = fscanf(file, "%d", &(game->m));
    if(res < 0){};
    res = fscanf(file, " "); /* clears whitespace */
    if(res < 0){
        printf("scan error!\n");
        fclose(file);
        return false;
    }
    res = fscanf(file, "%d", &(game->n));
    if(res < 0){};
    game->N = (game->n)*(game->m);
    board = (square*)calloc((size_t) (game->N * game->N), sizeof(square));
    if(!board){
        printf("malloc error!\n\n");
        fclose(file);
        return false;
    }
    for (i = 0; i < game->N; ++i) {
        for (j = 0; j < game->N; j++) {
            res = fscanf(file, " "); /* clears whitespace */
            if(res < 0){
                printf("scan error!\n");
                fclose(file);
                free(board);
                return false;
            }
            res = fscanf(file, "%d", &(board[i * (game->N) + j].value));
            if(res < 0){};
            fixed_c = (char)fgetc(file);
            board[i * (game->N) + j].state = normal;
            if(fixed_c == '.' && mode != edit){
                board[i * (game->N) + j].state = fixed;
            }
        }
    }
    game->board = board;
    fclose(file);
    return true;
}