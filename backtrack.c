
#include <memory.h>
#include "backtrack.h"
#include "legal_vals_list.h"



int num_solutions(sudoku *game) {
    int i, last_square;
    int curr_ind = 0;
    int total_solutions = 0;
    bool going_down = true;
    square *board_backup = (square *) calloc((size_t) (game->N * game->N), sizeof(square));
    LegalValsNode **legal_values = (LegalValsNode **) calloc((size_t) (game->N * game->N), sizeof(LegalValsNode *));
    if (!board_backup) {
        return -1;
    }
    if (!legal_values) {
        free(board_backup);
        return -1;
    }
    memcpy(board_backup, game->board, sizeof(square) * game->N * game->N);
    last_square = get_last_empty_square(game);
    do {
        if (board_backup[curr_ind].value != 0) { /*pre-filled square, continue*/
            curr_ind += going_down ? 1 : -1;
            continue;
        }
        if (!going_down && legal_values[curr_ind] == NULL) {/*going up through exhausted square*/
            game->board[curr_ind].value = 0;
            curr_ind--;
            continue;
        }
        if (curr_ind == last_square) {/*last square, count solutions*/
            total_solutions += count_legal_vals(curr_ind, game);
            curr_ind--;
            going_down = false;
            continue;
        }
        if (legal_values[curr_ind] == NULL) {/*entering new square, fill values list*/
            fill_legal_vals(curr_ind, game, legal_values);
        }
        if (!legal_values[curr_ind]) {/*no legal values, go up*/
            going_down = false;
            curr_ind--;
            continue;
        }
        game->board[curr_ind].value = get_next_legal_value(legal_values, curr_ind); /*get next legal value and go down*/
        curr_ind++;
        going_down = true;
    } while (curr_ind >= 0);

    memcpy(game->board, board_backup, sizeof(square) * game->N * game->N);
    free(board_backup);
    for (i = 0; i < game->N * game->N; ++i) {
        free_legal_vals_list(legal_values[i]);
    }
    free(legal_values);
    return total_solutions;
}

int get_last_empty_square(sudoku *game) {
    int i;
    for (i = (game->N * game->N) - 1; i >= 0; --i) {
        if (game->board[i].value == 0) {
            return i;
        }
    }
    return -1;
}

int get_next_legal_value(LegalValsNode **legal_values, int ind) {
    int value = legal_values[ind]->value;
    LegalValsNode *tmp = legal_values[ind];
    legal_values[ind] = legal_values[ind]->next;
    free(tmp);
    return value;
}

void fill_legal_vals(int ind, sudoku *game, LegalValsNode **legal_values) {
    int i;
    for (i = 1; i <= game->N; ++i) {
        if (is_legal_value(game, ind / game->N, ind % game->N, i)) {
            legal_values[ind] = add_legal_value(i, legal_values[ind]);
        }
    }
}

int count_legal_vals(int ind, sudoku *game) {
    int i, total = 0;
    for (i = 1; i <= game->N; ++i) {
        if (is_legal_value(game, ind / game->N, ind % game->N, i)) {
            total++;
        }
    }
    return total;
}
