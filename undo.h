
#ifndef PROJECTO_PROJECT_UNDO_H
#define PROJECTO_PROJECT_UNDO_H

#include "sudoku_game.h"
#include <stdio.h>
#include <stdlib.h>

/**
 * undo a move and print changes made to user (if requested_
 *
 * @param game the game where the move will be undone
 * @param print_changes
 */
bool undo(sudoku *game, bool print_changes);


/**
 * redo a move and print changes made to user (if requested)
 *
 * @param game the game where the move will be redone
 * @param print_changes
 */
bool redo(sudoku *game, bool print_changes);

/**
 * applies a change node to the game
 *
 * @param game the game which will be changed
 * @param node the change node
 * @param is_redo whether to redo or undo
 * @param print_changes
 */
void apply_move(sudoku *game, ChangeNode* node, bool is_redo, bool print_changes);

#endif /*PROJECTO_PROJECT_UNDO_H*/
