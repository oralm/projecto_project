
#include <stdio.h>
#include "guesses.h"
#include "lin_prog.h"
#include "undo.h"

int hint(sudoku *game, int row, int col) {
    int i, N, return_val;
    double *result = 0;
    N = game->N;
    if ((return_val = solve_full_board(game, true, &result)) != 0) {
        free(result);
        if (return_val == 1) {
            return -2;
        }
        return return_val;
    }
    for (i = 0; i < game->N; ++i) {
        if (result[MAPIND(row, col, i)] > 0.5) {
            free(result);
            return i + 1;
        }
    }
    free(result);
    return -1;
}

int guess_hint(sudoku *game, int row, int col) {
    int i, N, return_val;
    double *result = 0;
    N = game->N;
    if ((return_val = solve_full_board(game, false, &result)) != 0) {
        free(result);
        return return_val;
    }
    for (i = 0; i < game->N; ++i) {
        if (result[MAPIND(row, col, i)] > 0.0) {
            printf("chance of value %d: %f\n", i + 1, result[MAPIND(row, col, i)]);
        }
    }
    free(result);
    return 0;
}

int guess(sudoku *game, double X) {
    int i, j, k, N, picked_val, res;
    bool found;
    struct commandnode *new_command;
    double rand_val;
    double *result = 0;
    picked_val = 0;
    N = game->N;
    res = solve_full_board(game, false, &result);
    if (res != 0) {
        free(result);
        return res;
    }
    new_command = add_command(game->current_command);
    if (new_command == NULL) {
        free(result);
        return -1;
    }
    game->current_command = new_command;
    for (i = 0; i < N; i++) {
        for (j = 0; j < N; j++) {
            found = false;
            for (k = 0; k < N; k++) {
                if (result[MAPIND(i, j, k)] > X) {
                    found = true;
                }
            }
            if (!found) {
                free(result);
                undo(game, false);
                return 1;
            }
        }
    }
    for (i = 0; i < N; i++) {
        for (j = 0; j < N; j++) {
            rand_val = ((double) rand()) / ((double) RAND_MAX);
            for (k = 0; k < N; k++) {
                rand_val -= result[MAPIND(i, j, k)];
                picked_val = k + 1;
                if (rand_val < 0) {
                    break;
                }
            }
            if (is_legal_value(game, i, j, picked_val)) {
                if (!add_change(new_command, i, j, get_square_value(game, i, j), picked_val)) {
                    free(result);
                    undo(game, false);
                    return -1;
                }
                set_square_value(game, i, j, picked_val);
            }
        }
    }
    free(result);
    return 0;
}
