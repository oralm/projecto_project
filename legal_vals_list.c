
#include <stdlib.h>
#include "legal_vals_list.h"

LegalValsNode *add_legal_value(int value, LegalValsNode *node) {
    LegalValsNode *tmp = node;
    if (node == NULL) {
        node = (LegalValsNode *) malloc(sizeof(LegalValsNode));
        node->value = value;
        node->next = NULL;
        return node;
    }
    while (node->next != NULL) {
        node = node->next;
    }
    node->next = (LegalValsNode *) malloc(sizeof(LegalValsNode));
    if (!node->next) {
        return NULL;
    }
    node->next->value = value;
    node->next->next = NULL;
    return tmp;
}

void free_legal_vals_list(LegalValsNode *node) {
    LegalValsNode *tmp;

    while (node != NULL) {
        tmp = node;
        node = node->next;
        free(tmp);
    }
}
