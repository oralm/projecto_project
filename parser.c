
#include <unistd.h>
#include "parser.h"
#include "guesses.h"
#include "generate.h"

#define ERROR_MESSAGE(func_name) printf("Error: %s has failed\n", func_name)
#define ERROR_INVAL_COMMAND(reason) printf("Invalid command: %s\n", reason)
#define WRONG_MODE(modes) printf("Command unavailable in current mode. It is available in %s mode(s)\n", modes)


/**
 * Checks if current board has any illegal values
 * @param game
 * @return existence of illegal value
 */
bool check_if_illegal_values(sudoku *game) {
    int i, j, val;
    for (i = 0; i <game->N; i++){
        for (j = 0; j < game->N; j++){
            val = get_square_value(game, i, j);
            if (val < 0 || val > game->N){
                return true;
            }
        }
    }
    return false;
}

/**
 * Checks if string arg matches integer X
 * (no trailing characters to the integer in arg)
 * @param X
 * @param arg
 * @return true if matches
 */
bool was_input_only_numbers(int X, char *arg) {
    char number[10];
    sprintf(number, "%d", X);
    if (strcmp(number, arg) == 0) {
        return true;
    }
    return false;
}

bool was_input_only_double(double X, char *arg) {
    char number[MAX_COMMAND_LENGTH];
    sprintf(number, "%g", X);
    if (strcmp(number, arg) == 0) {
        return true;
    }
    return false;
}

/**
 * implements reset command
 * return 0 on success
 * */
int reset_command(sudoku *game) {
    while (!game->current_command->is_head) {
        undo(game, false);
    }
    print_board(game);
    return 0;
}

/**
 * implements autofill command
 * return 0 on success
 * */
int autofill_command(sudoku *game) {
    int row, col, old_value, new_value;
    CommandNode *auto_fill_command;
    if (!valid_board(game)) {
        ERROR_INVAL_COMMAND("cannot autofill when errors in board");
        return 0;
    }
    auto_fill_command = add_command(
            game->current_command); /* adding all future changes to node and then "applying" them with redo */
    if (auto_fill_command == NULL) {
        ERROR_MESSAGE("autofill_command");
        return 1;
    }
    for (row = 0; row < game->N; row++) {
        for (col = 0; col < game->N; col++) {
            if ((old_value = get_square_value(game, row, col)) == 0) {
                new_value = find_only_legal_value(game, row, col);
                if (new_value != 0) { /* means new_value is the only legal value in cell */
                    if (!add_change(auto_fill_command, row, col, old_value, new_value)) {
                        ERROR_MESSAGE("autofill_command");
                        return 1; /* means malloc error */
                    }
                }
            }
        }
    }
    redo(game, false);
    update_errors(game);
    print_board(game);
    check_if_solved(game);
    return 0;
}

/**
 * implements num_solutions command
 * return 0 on success
 * */
int num_solutions_command(sudoku *game) {
    int result;
    update_errors(game);
    if (!valid_board(game)) {
        ERROR_INVAL_COMMAND("can't count solutions in erroneous board");
        return 0;
    }
    if (is_board_full(game)) {
        update_errors(game);
        result = 0;
        if (valid_board(game)) {
            result = 1;
        }
    } else if ((result = num_solutions(game)) < 0) {
        ERROR_MESSAGE("num_solutions_command");
        return 1;
    }
    printf("%d\n", result);
    return 0;
}


/**
 * implements guess_hint command
 * return 0 on success
 * */
int guess_hint_command(sudoku *game, int X, int Y) {
    if (X < 0 || X >= game->N || Y < 0 || Y >= game->N) {
        ERROR_INVAL_COMMAND("row and column must be numbers between 1 and N");
        return 0;
    }
    update_errors(game);
    if (!valid_board(game)) {
        ERROR_INVAL_COMMAND("can not guess hint when board is erroneous");
        return 0;
    }
    if (get_square_state(game, X, Y) == fixed || get_square_value(game, X, Y) != 0) {
        ERROR_INVAL_COMMAND("can not hint fixed or filled cells");
        return 0;
    }
    if (guess_hint(game, X, Y) == -1) {
        ERROR_INVAL_COMMAND("board is unsolvable");
        return 0;
    }
    return 0;
}

/**
 * implements hint command
 * return 0 on success
 * */
int hint_command(sudoku *game, int X, int Y) {
    int result;
    if (X < 0 || X >= game->N || Y < 0 || Y >= game->N) {
        ERROR_INVAL_COMMAND("row and column must be numbers between 1 and N");
        return 0;
    }
    update_errors(game);
    if (!valid_board(game)) {
        ERROR_INVAL_COMMAND("can not hint when board is erroneous");
        return 0;
    }
    if (get_square_state(game, X, Y) == fixed || get_square_value(game, X, Y) != 0) {
        ERROR_INVAL_COMMAND("can not hint fixed or filled cells");
        return 0;
    }
    result = hint(game, X, Y);
    if (result == -2) {
        ERROR_INVAL_COMMAND("board is unsolvable");
        return 0;
    }
    if (result == -1){
        ERROR_MESSAGE("hint_command");
        return 1;
    }
    printf("Hint: set <%d,%d> to %d\n", X + 1, Y + 1, result);
    return 0;
}

/**
 * implements save command
 * return 0 on success
 * */
int save_command(sudoku *game, char *X) {
    if (!save(game, X)) {
        ERROR_MESSAGE("save_command's file opening");
        return 0;
    }
    return 0;
}

/**
 * implements redo command
 * return 0 on success
 * */
int redo_command(sudoku *game) {
    if (redo(game, true)) {
        print_board(game);
        check_if_solved(game);
        return 0;
    }
    ERROR_INVAL_COMMAND("No command to redo");
    return 0;
}

/**
 * implements undo command
 * return 0 on success
 * */
int undo_command(sudoku *game) {
    if (undo(game, true)) {
        print_board(game);
        return 0;
    }
    ERROR_INVAL_COMMAND("no command to undo");
    return 0;
}

/**
 * implements generate command
 * return 0 on success
 * */
int generate_command(sudoku *game, int X, int Y) {
    int i, j, res;
    int num_of_empty_cells = 0;
    for (i = 0; i < game->N; i++) {
        for (j = 0; j < game->N; j++) {
            if (get_square_value(game, i, j) == 0) {
                num_of_empty_cells++;
            }
        }
    }
    if (X < 0 || X >= (game->N * game->N) || Y < 0 || Y >= (game->N * game->N)) {
        ERROR_INVAL_COMMAND("inputs must be numbers between 1 and N^2");
        return 0;
    }
    if (X > num_of_empty_cells) {
        ERROR_INVAL_COMMAND("not enough empty squares to fill");
        return 0;
    }
    update_errors(game);
    if (!valid_board(game)) {
        ERROR_INVAL_COMMAND("can not generate when board is erroneous");
        return 0;
    }
    res = generate(game, X, Y);
    if (res == 0) {
        update_errors(game);
        print_board(game);
        return 0;
    }
    if (res == -1) {
        ERROR_MESSAGE("generate_command");
        return 1;
    }
    if (res == -2) {
        ERROR_INVAL_COMMAND("board is unsolvable");
        return 0;
    }
    return 0;
}

/**
 * implements guess command
 * return 0 on success
 * */
int guess_command(sudoku *game, double X) {
    int return_val;
    update_errors(game);
    if (X < 0.0 || X > 1.0) {
        ERROR_INVAL_COMMAND("threshold must be between 0 and 1");
        return 0;
    }
    if (!valid_board(game)) {
        ERROR_INVAL_COMMAND("can not guess when board is erroneous");
        return 0;
    }
    if ((return_val = guess(game, X)) == -1) {
        ERROR_MESSAGE("guess_command");
        return 1;
    }
    if (return_val == 1){
        ERROR_INVAL_COMMAND("no good enough guess");
        return 0;
    }
    update_errors(game);
    print_board(game);
    check_if_solved(game);
    return 0;
}

/**
 * implements validate command
 * return 0 on success
 * */
int validate_command(sudoku *game) {
    int result;
    update_errors(game);
    if (!valid_board(game)) {
        ERROR_INVAL_COMMAND("can not validate when board is erroneous");
        return 0;
    }
    result = validate(game);
    if (result == 0) {
        printf("Board is solvable.\n");
        return 0;
    }
    if (result == 1) {
        printf("Board is unsolvable\n");
        return 0;
    }
    ERROR_MESSAGE("validate_command");
    return 1;
}

/**
 * implements set command
 * @param game
 * @param X - zero based row number
 * @param Y - zero based column number
 * @param Z - value from 1 to N (or 0 for blank)
 * @return 0 on success 1 on errors
 */
int set_command(sudoku *game, int X, int Y, int Z) {
    CommandNode *new_command;
    int row = X;
    int col = Y;
    if (row < 0 || row >= game->N || col < 0 || col >= game->N) {
        ERROR_INVAL_COMMAND("row and column must be numbers between 1 and N");
        return 0;
    }
    if (game->mode == solve && get_square_state(game, row, col) == fixed) {
        ERROR_INVAL_COMMAND("fixed cells cannot be changed in solve mode");
        return 0;
    }
    if (Z < 0 || Z > game->N) {
        ERROR_INVAL_COMMAND("cell values must be numbers 1 <= Z <= N, or 0 to empty cell");
        return 0;
    }

    new_command = add_command(game->current_command);
    if (new_command == NULL) {
        ERROR_MESSAGE("set_command");
        return 1;
    }
    game->current_command = new_command;
    if (!add_change(new_command, row, col, get_square_value(game, row, col), Z)) {
        ERROR_MESSAGE("set_command");
        return 1;
    }
    set_square_value(game, row, col, Z);
    update_errors(game);
    print_board(game);
    check_if_solved(game);
    return 0;
}

/**
 * implements print_board command
 * return 0 on success
 * */
int print_board_command(sudoku *game) {
    print_board(game);
    return 0;
}

/**
 * implements mark_errors command
 * return 0 on success
 * */
int mark_errors_command(sudoku *game, char *X) {
    if (strcmp(X, "0") != 0 && strcmp(X, "1") != 0) {
        ERROR_INVAL_COMMAND("mark errors takes input 0 (no) or 1 (yes)");
        return 0;
    }
    game->mark_errors = (int) strtol(X, NULL, 10);
    return 0;
}



/**
 * implements edit command
 * return 0 on success
 * */
int edit_command(sudoku *game, char *X) {
    FILE *file;
    if (X == NULL) {
        free_game(game);
        if (!new_board(game)) {
            ERROR_MESSAGE("edit_command");
            return 1;
        }
    } else{
        file = fopen(X, "r");
        if (file == NULL) {
            ERROR_INVAL_COMMAND("file not found");
            return 0;
        }
        fclose(file);
        free_game(game);
        if (!load(game, X, edit)) {
            printf("Error - file failed.\n");
            return 1;
        }
        if(game->N == 0 || check_if_illegal_values(game)){
            printf("File was of incorrect format or had out of range values.\nGame set to init mode.\n");
            game->mode = init;
            return 0;
        }
    }
    update_errors(game);
    print_board(game);
    return 0;
}

/**
 * implements solve command
 * return 0 on success
 * */
int solve_command(sudoku *game, char *X) {
    FILE *file;
    file = fopen(X, "r");
    if (file == NULL) {
        ERROR_INVAL_COMMAND("file not found");
        return 0;
    }
    fclose(file);
    free_game(game);
    if (!load(game, X, solve)) {
        printf("Error - file failed.\n");
        return 1;
    }
    if(game->N == 0 || check_if_illegal_values(game)){
        printf("File was of incorrect format or had out of range values.\nGame set to init mode.\n");
        game->mode = init;
        return 0;
    }
    update_errors(game);
    print_board(game);
    check_if_solved(game);
    return 0;
}

int parse_command(sudoku *game, char *command_buffer) {
    char *command_word, *arg_a, *arg_b, *arg_c, *arg_d;
    char temp[2];
    char* Werror;
    int X, Y, Z;
    int i;
    double fX;
    char *command_line = command_buffer; /* one extra for null char and one to check if more than 256 chars read */
    for (i = 0; i < MAX_COMMAND_LENGTH + 2; i++) {
        command_line[i] = ' ';
    }
    if (feof(stdin)) { /* no more input. no more bloodshed. */
        printf("Exiting...\n");
        return -1;
    }
    printf("Enter command: ");
    if (fgets(command_line, MAX_COMMAND_LENGTH + 2, stdin) == NULL && !feof(stdin)) { /* reading command to buffer */
        ERROR_MESSAGE("parse_command");
        return -1;
    }
    if (command_line[MAX_COMMAND_LENGTH + 1] ==
        '\0') { /* means maximum + 2 chars where read including terminator, so command exceeded max */
        ERROR_INVAL_COMMAND("exceeded max length of command");
        temp[0] = 'a';
        temp[1] = '\0';
        while (temp[0] != '\n' && temp[0] != '\0' && !feof(stdin)) { /* loop through rest of this invalid input line*/
            Werror = fgets(temp, 2, stdin);
            if(&Werror == &command_line){
                break; /*never happens, Werror is horrible*/
            }
        }
        return 0;
    }

    command_word = strtok(command_line, " \t\r\n");
    arg_a = strtok(NULL, " \t\r\n");
    arg_b = strtok(NULL, " \t\r\n");
    arg_c = strtok(NULL, " \t\r\n");
    arg_d = strtok(NULL, " \t\r\n");
    if (command_word == NULL) { /* command had only whitespace */
        return 0;
    }

    if (strcmp(command_word, "exit") == 0) {
        if (arg_a != NULL) {
            ERROR_INVAL_COMMAND("exit takes no parameters");
            return 0;
        }
        printf("Exiting...\n");
        return -1;
    }

    if (strcmp(command_word, "reset") == 0) {
        if (game->mode != edit && game->mode != solve) {
            WRONG_MODE("edit and solve");
            return 0;
        }
        if (arg_a != NULL) {
            ERROR_INVAL_COMMAND("reset takes no parameters");
            return 0;
        }
        return reset_command(game);
    }

    if (strcmp(command_word, "autofill") == 0) {
        if (game->mode != solve) {
            WRONG_MODE("solve");
            return 0;
        }
        if (arg_a != NULL) {
            ERROR_INVAL_COMMAND("autofill takes no parameters");
            return 0;
        }
        return autofill_command(game);
    }

    if (strcmp(command_word, "num_solutions") == 0) {
        if (game->mode != edit && game->mode != solve) {
            WRONG_MODE("edit and solve");
            return 0;
        }
        if (arg_a != NULL) {
            ERROR_INVAL_COMMAND("num_solutions takes no parameters");
            return 0;
        }
        return num_solutions_command(game);
    }

    if (strcmp(command_word, "guess_hint") == 0 || strcmp(command_word, "hint") == 0) {
        if (game->mode != solve) {
            WRONG_MODE("solve");
            return 0;
        }
        if (arg_b == NULL || arg_c != NULL) {
            ERROR_INVAL_COMMAND("guess_hint and hint take two parameters");
            return 0;
        }
        X = atoi(arg_a) - 1;
        Y = atoi(arg_b) - 1;
        /* check if there where trailing chars to the integers in args */
        if (!was_input_only_numbers(X + 1, arg_a) || !was_input_only_numbers(Y + 1, arg_b)) {
            X = -1; /* will trigger row/column error message in hint_command*/
        }
        if (strcmp(command_word, "hint") == 0) {
            return hint_command(game, X, Y);
        }
        return guess_hint_command(game, X, Y);
    }

    if (strcmp(command_word, "save") == 0) {
        if (game->mode != edit && game->mode != solve) {
            WRONG_MODE("edit and solve");
            return 0;
        }
        if (arg_a == NULL || arg_b != NULL) {
            ERROR_INVAL_COMMAND("save takes one parameter");
            return 0;
        }
        return save_command(game, arg_a);
    }

    if (strcmp(command_word, "redo") == 0) {
        if (game->mode != edit && game->mode != solve) {
            WRONG_MODE("edit and solve");
            return 0;
        }
        if (arg_a != NULL) {
            ERROR_INVAL_COMMAND("redo takes no parameters");
            return 0;
        }
        return redo_command(game);
    }

    if (strcmp(command_word, "undo") == 0) {
        if (game->mode != edit && game->mode != solve) {
            WRONG_MODE("edit and solve");
            return 0;
        }
        if (arg_a != NULL) {
            ERROR_INVAL_COMMAND("undo takes no parameters");
            return 0;
        }
        return undo_command(game);
    }

    if (strcmp(command_word, "generate") == 0) {
        if (game->mode != edit) {
            WRONG_MODE("edit");
            return 0;
        }
        if (arg_b == NULL || arg_c != NULL) {
            ERROR_INVAL_COMMAND("generate takes two parameters");
            return 0;
        }
        X = atoi(arg_a);
        Y = atoi(arg_b);
        /* check if there where trailing chars to the integers in args */
        if (!was_input_only_numbers(X, arg_a) || !was_input_only_numbers(Y, arg_b)) {
            X = -1;
        }
        return generate_command(game, X, Y);
    }

    if (strcmp(command_word, "guess") == 0) {
        if (game->mode != solve) {
            WRONG_MODE("solve");
            return 0;
        }
        if (arg_a == NULL || arg_b != NULL) {
            ERROR_INVAL_COMMAND("guess takes one parameter");
            return 0;
        }
        fX = atof(arg_a);
        /* check if there where trailing chars to the float in arg */
        if (!was_input_only_double(fX, arg_a)) {
            fX = -1;
        }
        return guess_command(game, fX);
    }

    if (strcmp(command_word, "validate") == 0) {
        if (game->mode != edit && game->mode != solve) {
            WRONG_MODE("edit and solve");
            return 0;
        }
        if (arg_a != NULL) {
            ERROR_INVAL_COMMAND("validate takes no parameters");
            return 0;
        }
        return validate_command(game);
    }

    if (strcmp(command_word, "set") == 0) {
        if (game->mode != edit && game->mode != solve) {
            WRONG_MODE("edit and solve");
            return 0;
        }
        if (arg_c == NULL || arg_d != NULL) {
            ERROR_INVAL_COMMAND("set takes three parameters");
            return 0;
        }
        X = atoi(arg_a) - 1;
        Y = atoi(arg_b) - 1;
        Z = atoi(arg_c);
        /* check if there where trailing chars to the integers in args */
        if (!was_input_only_numbers(X + 1, arg_a) || !was_input_only_numbers(Y + 1, arg_b)) {
            X = -1; /* will trigger row/column error message in set_command*/
        }
        if (!was_input_only_numbers(Z, arg_c)) {
            Z = -1;
        }
        return set_command(game, X, Y, Z);
    }

    if (strcmp(command_word, "print_board") == 0) {
        if (game->mode != edit && game->mode != solve) {
            WRONG_MODE("edit and solve");
            return 0;
        }
        if (arg_a != NULL) {
            ERROR_INVAL_COMMAND("print_board takes no parameters");
            return 0;
        }
        return print_board_command(game);
    }

    if (strcmp(command_word, "mark_errors") == 0) {
        if (game->mode != solve) {
            WRONG_MODE("solve");
            return 0;
        }
        if (arg_a == NULL || arg_b != NULL) {
            ERROR_INVAL_COMMAND("mark_errors takes one parameter");
            return 0;
        }
        return mark_errors_command(game, arg_a);
    }

    if (strcmp(command_word, "edit") == 0) {
        if (arg_b != NULL) {
            ERROR_INVAL_COMMAND("edit takes zero or one parameters");
            return 0;
        }
        return edit_command(game, arg_a);
    }

    if (strcmp(command_word, "solve") == 0) {
        if (arg_a == NULL || arg_b != NULL) {
            ERROR_INVAL_COMMAND("solve takes one parameter");
            return 0;
        }
        return solve_command(game, arg_a);
    }

    ERROR_INVAL_COMMAND("command word not recognised"); /* case when first command word was not identified */
    return 0;
}