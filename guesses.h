
#ifndef PROJECTO_PROJECT_GUESSES_H
#define PROJECTO_PROJECT_GUESSES_H

#include "sudoku_game.h"

/**
 * guesses the value of a square with ILP.
 * make sure to check that the square is empty and the board isn't wrong.
 *
 * @param game: game to check
 * @param row: square row
 * @param col: square col
 * @return value, -1 if fatal error, -2 if no solution
 */
int hint(sudoku *game, int row, int col);

/**
 * prints possible values of a square with LP.
 * make sure to check that the square is empty and the board isn't wrong.
 *
 * @param game: game to check
 * @param row: square row
 * @param col: square col
 * @return success = 1; no solution = 1; fatal error = -1
 */
int guess_hint(sudoku *game, int row, int col);

/**
 * guesses a (maybe partial) solution using ILP
 * @param game game
 * @param X threshold, 0 < X < 1
 * @return -1 if malloc fail, 0 if success, 1 if not found
 */
int guess(sudoku *game, double X);

#endif /*PROJECTO_PROJECT_GUESSES_H*/
