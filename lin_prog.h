#ifndef PROJECTO_PROJECT_LIN_PROG_H
#define PROJECTO_PROJECT_LIN_PROG_H

#include <stdbool.h>
#include "sudoku_game.h"

/**
 * returns the index in the flattened 1D map array given a row, col, and val.
 */
#define MAPIND(row, col, val) ((row)*N*N + (col)*N + (val))

/**
 * uses (I)LP to solve a board, returns result in result_board.
 *
 * @param game: the game to solve
 * @param is_ILP: whether to use ILP
 * @param result_board: a UNINITIALIZED pointer, will return an N*N*N solutions board. acess it with MAPIND
 * @return found solution = 0, no solution = 1, fatal error = -1
 */
int solve_full_board(sudoku *game, bool is_ILP, double **result_board);

/**
 * builds the constraint defined by a single column
 * @param map: the map
 * @param cons: pointer to an array that will be filled with constraints
 * @param col: the column
 * @param val: the value
 * @param N: game's N
 * @return number of vars in the constraint.
 */
int build_col_constraint(int *cons, int col, int val, int N);

/**
 * builds the constraint defined by a single row
 * @param map: the map
 * @param cons: pointer to an array that will be filled with constraints
 * @param row: the row
 * @param val: the value
 * @param N: game's N
 * @return number of vars in the constraint.
 */
int build_row_constraint(int *cons, int row, int val, int N);

/**
 * builds the constraint defined by a single value
 * @param map: the map
 * @param cons: pointer to an array that will be filled with constraints
 * @param row: the row
 * @param col: the column
 * @param N: game's N
 * @return number of vars in the constraint.
 */
int build_val_constraint(int *cons, int row, int col, int N);

/**
 * builds the constraint defined by a single square
 * @param map: the map
 * @param cons: pointer to an array that will be filled with constraints
 * @param square: square index
 * @param val: the value
 * @param m: game's m
 * @param n: game's n
 * @return number of vars in the constraint.
 */
int build_square_constraint(int *cons, int square, int val, int m, int n);

#endif /*PROJECTO_PROJECT_LIN_PROG_H*/
