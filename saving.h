
#ifndef PROJECTO_PROJECT_SAVING_H
#define PROJECTO_PROJECT_SAVING_H

#include "sudoku_game.h"
#include <stdio.h>
#include <stdlib.h>
/**
 * saves a game into a file, in the format specified by the assignment.
 * @param game the game to be saved
 * @param path the path to save it to
 * @return false if file error, true otherwise
 */
bool save(sudoku *game, char *path);

/**
 * loads a game from a given file
 *
 * @param game an UNINITIALIZED pointer, which will be initialized with the new game by the function
 * @param path a path to the file
 * @param mode edit/solve
 * @return success
 */
bool load(sudoku *game, char *path, Mode mode);

#endif /*PROJECTO_PROJECT_SAVING_H*/
