

#include "printboard.h"

void print_separator_row(sudoku *game) {
    int i;
    for(i = 0; i < 4*game->N + game->m + 1; i++){
        printf("-");
    }
    printf("\n");
}

void print_row(sudoku *game, int row) {
    int curr_cell, curr_cell_value;
    State curr_state;
    for(curr_cell = 0; curr_cell < game->N; curr_cell++){
        /* separator char every n cells */
        if (curr_cell % game->n == 0){
            printf("|");
        }

        /* adding cell's first three chars - one space and two chars for value */
        curr_cell_value = get_square_value(game, row, curr_cell);
        if(curr_cell_value == 0){
            printf("   ");
        }
        else{
            printf(" %2d", curr_cell_value);
        }

        /* adding fourth char to represent state */
        curr_state = get_square_state(game, row, curr_cell);
        if (curr_state == fixed){
            printf(".");
        }
        else if (curr_state == error  && (game->mode == edit || game->mark_errors == 1)){
            printf("*");
        }
        else{
            printf(" ");
        }
    }
    printf("|\n"); /* closing row */
}

void print_board(sudoku *game) {
    int curr_row;
    for(curr_row = 0; curr_row < game->N; curr_row++){
        if (curr_row % game->m == 0){
            print_separator_row(game);
        }
        print_row(game, curr_row);
    }
    print_separator_row(game);
}


