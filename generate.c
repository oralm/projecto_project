#include <memory.h>
#include "generate.h"
#include "backtrack.h"
#include "legal_vals_list.h"
#include "lin_prog.h"
#include "undo.h"

bool choose_legal_val(sudoku *game, int row, int col) {
    LegalValsNode *legal_list;
    LegalValsNode *head;
    int i, j, count;
    legal_list = NULL;
    count = 0;
    for (i = 1; i <= game->N; ++i) {
        if (is_legal_value(game, row, col, i)) {
            count++;
            legal_list = add_legal_value(i, legal_list);
        }
    }
    if (count == 0) {
        free_legal_vals_list(legal_list);
        return false;
    }
    j = rand() % count;
    head = legal_list;
    for (i = 0; i < j - 1; ++i) {
        legal_list = legal_list->next;
    }
    set_square_value(game, row, col, legal_list->value);
    free_legal_vals_list(head);
    return true;
}

bool fill_cells(sudoku *game, int X) {
    int i, j;
    while (X > 0) {
        i = rand() % game->N;
        j = rand() % game->N;
        if (get_square_value(game, i, j) != 0) {
            continue;
        }
        if (!choose_legal_val(game, i, j))
            return false;
        X--;
    }
    return true;
}

int solve_board(sudoku *game) {
    int i, j, k, N, return_val;
    double *result;
    bool found;
    result = NULL;
    N = game->N;
    if ((return_val = solve_full_board(game, true, &result)) == -1) {
        free(result);
        return -1;
    }
    if (return_val == 1) {
        free(result);
        return 1;
    }
    for (i = 0; i < game->N; i++) {
        for (j = 0; j < game->N; j++) {
            if (get_square_value(game, i, j) != 0) {
                continue;
            }
            found = false;
            for (k = 0; k < game->N; ++k) {
                if (result[MAPIND(i, j, k)] > 0.5) {
                    found = true;
                    set_square_value(game, i, j, k + 1);
                    break;
                }
            }
            if (!found) {
                free(result);
                return 1;
            }
        }
    }
    free(result);
    return 0;
}

int generate(sudoku *game, int X, int Y) {
    int i, j, old, new, attempts, return_val;
    CommandNode *new_command;
    square *board_backup;
    board_backup = (square *) calloc((size_t) (game->N * game->N), sizeof(square));
    if (!board_backup) {
        return -1;
    }
    memcpy(board_backup, game->board, sizeof(square) * game->N * game->N);

    for (attempts = 0; attempts < 1000; attempts++) {
        memcpy(game->board, board_backup, sizeof(square) * game->N * game->N);
        if (!fill_cells(game, X)) {
            continue;
        }
        if ((return_val = solve_board(game)) == 1) { /* no solution found */
            continue;
        }
        if (return_val == -1) { /* malloc error */
            free(board_backup);
            return -1;
        }
        while (Y < game->N * game->N) {
            i = rand() % game->N;
            j = rand() % game->N;
            if (get_square_value(game, i, j) != 0) {
                set_square_value(game, i, j, 0);
                Y++;
            }
        }
        new_command = add_command(game->current_command);
        if (new_command == NULL) {
            free(board_backup);
            return -1;
        }
        game->current_command = new_command;
        for (i = 0; i < game->N; i++) {
            for (j = 0; j < game->N; j++) {
                old = board_backup[i * game->N + j].value;
                new = get_square_value(game, i, j);
                if (!add_change(new_command, i, j, old, new)) {
                    undo(game, false);
                    free(board_backup);
                    return -1;
                }
            }
        }
        free(board_backup);
        return 0;
    }
    memcpy(game->board, board_backup, sizeof(square) * game->N * game->N);
    free(board_backup);
    return -2;
}

int validate(sudoku *game) {
    int res;
    square *board_backup;
    board_backup = (square *) calloc((size_t) (game->N * game->N), sizeof(square));
    if (!board_backup) {
        return -1;
    }
    memcpy(board_backup, game->board, sizeof(square) * game->N * game->N);
    res = solve_board(game);
    memcpy(game->board, board_backup, sizeof(square) * game->N * game->N);
    free(board_backup);
    return res;

}