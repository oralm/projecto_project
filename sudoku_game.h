

#ifndef PROJECTO_PROJECT_SUDOKU_GAME_H
#define PROJECTO_PROJECT_SUDOKU_GAME_H

#include "list.h"
#include <stdlib.h>
#include <stdbool.h>


typedef enum State {
    normal, fixed, error
} State;

typedef enum Mode {
    init, edit, solve
} Mode;

/**
 * a single square on the board. saves the state and value of the square.
 */
typedef struct square {
    State state;
    int value;
} square;

/**
 * the game board.
 *
 * @param m single square width
 * @param n single square hight
 * @param N board side length in squares
 * @param board pointer to an array of squares, the board itself
 * @param is_solved is the board fully solved
 * @param mode current editing mode
 * @param current_command current command in undo/redo list
 * @param mark_errors is mark errors on
 */
typedef struct sudoku {
    int m;
    int n;
    int N;
    square *board;
    Mode mode;
    CommandNode *current_command;
    int mark_errors;
} sudoku;

/**
 * Wrapper function for setting specified square in board to a given value
 * @param game
 * @param row in board of square to be modified
 * @param col in board of square to be modified
 * @param value to be written in square
 */
void set_square_value(sudoku *game, int row, int col, int value);

/**
 * Wrapper function for getting value of specified square in board
 * @param game
 * @param row in board of the square
 * @param col in board of the square
 * @return value that was held in specified square
 */
int get_square_value(sudoku *game, int row, int col);

/**
 * Wrapper function for setting specified square in board to a given state
 * @param game
 * @param row in board of square to be modified
 * @param col in board of square to be modified
 * @param state to be written in square
 */
void set_square_state(sudoku *game, int row, int col, State state);

/**
 * Wrapper function for getting state of specified square in board
 * @param game
 * @param row in board of the square
 * @param col in board of the square
 * @return state that was held in specified square
 */
State get_square_state(sudoku *game, int row, int col);

/**
 * updates state of all unfixed cells to error (if erroneous) and to normal (if valid)
 * @param game
 */
void update_errors(sudoku *game);

/**
 * Checks if there is any erroneous cell in board.
 * @param game
 * @return false if at least one error, true no error
 */
bool valid_board(sudoku *game);

/**
 * Checks if given value is legal in current board state.
 * @param game
 * @param row of cell
 * @param col of cell
 * @param value to be checked
 * @return true if legal, false illegal
 */
bool is_legal_value(sudoku *game, int row, int col, int value);

/**
 * Finds the *only* legal value of a given cell - if there is more than one legal value returns 0.
 * @param game
 * @param row of cell
 * @param col of cell
 * @return legal value of the cell, 0 if more than one legal value
 */
int find_only_legal_value(sudoku *game, int row, int col);

/**
 * creates a new and empty 9x9 board in edit mode
 * @param game
 * @return success
 */
bool new_board(sudoku *game);

/**
 * frees resources of game's board and command-list and sets them to null.
 * @param game
 */
void free_game(sudoku *game);

/**
 * Checks if board is full
 * @param game
 * @return true if (hummus) full
 */
bool is_board_full(sudoku *game);

/**
 * Checks if board is solved, prints proper message, and updates game mode if solved.
 * @param game
 */
void check_if_solved(sudoku *game);

#endif /*PROJECTO_PROJECT_SUDOKU_GAME_H*/
