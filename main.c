#include "parser.h"
#include "backtrack.h"


int main(){
    char *command_buffer;
    sudoku *game = (sudoku *) calloc(1, sizeof(sudoku));
    if (game == NULL){
        printf("Malloc error in main\b");
        return 1;
    }
    game->mode = init;
    printf("      'Sudoku is the art of finding a solution.' - Steve Jobs 1989\n");
    printf("        Welcome dear friends, to the ULTiMaTe SuDoKo PLaTFoRM(TM)\n");
    printf("                   A cutting edge Sudoku(C) experience.\n");
    printf("                                 ~ v5.2 ~\n");
    command_buffer = (char *)calloc(1,  MAX_COMMAND_LENGTH + 1 + 1);
    while(parse_command(game, command_buffer) == 0){}
    free_game(game);
    free(command_buffer);
    free(game);
    return 0;
}