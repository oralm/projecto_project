
#ifndef PROJECTO_PROJECT_PARSER_H
#define PROJECTO_PROJECT_PARSER_H


#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include "sudoku_game.h"
#include "saving.h"
#include "printboard.h"
#include "undo.h"
#include "backtrack.h"
#define MAX_COMMAND_LENGTH 256

/**
 * Parses one line of command from standard input to command_buffer and executes it.
 * command and arguments parsed and executed according to instructions in assignment.
 * at end of function command has been executed and game updated as neccesary.
 * @param game
 * @param command_buffer for input from stdin
 * @return 0 if program should proceed to another command, 1 if program should exit (both for exit and error)
 */
int parse_command(sudoku *game, char *command_buffer);

#endif /*PROJECTO_PROJECT_PARSER_H*/
