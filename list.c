
#include <stdio.h>
#include "list.h"

void free_change_list(ChangeNode *node) {
    ChangeNode *tmp;
    while (node != NULL) {
        tmp = node;
        node = node->next;
        free(tmp);
    }
}

void free_command_list(CommandNode *node) {
    CommandNode *tmp;

    while (node != NULL) {
        tmp = node;
        node = node->next;
        free_change_list(tmp->changes);
        free(tmp);
    }
}

CommandNode *add_command(CommandNode *prev_command) {
    CommandNode *curr_node;
    free_command_list(prev_command->next);
    prev_command->next = (CommandNode *) calloc(1, sizeof(CommandNode));
    curr_node = prev_command->next;
    if (curr_node == NULL) {
        return curr_node;
    }
    curr_node->prev = prev_command;
    curr_node->is_head = false;
    return curr_node;
}

bool add_change(CommandNode *current_command, int x, int y, int old_val, int new_val) {
    ChangeNode *curr = current_command->changes;
    if (curr == NULL) {
        current_command->changes = (ChangeNode *) calloc(1, sizeof(ChangeNode));
        curr = current_command->changes;
    } else {
        while (curr->next != NULL) {
            curr = curr->next;
        }
        curr->next = (ChangeNode *) calloc(1, sizeof(ChangeNode));
        curr = curr->next;
    }
    if (curr == NULL) {
        return false;
    }
    curr->x = x;
    curr->y = y;
    curr->after = new_val;
    curr->before = old_val;
    return true;
}

void free_full_command_list(CommandNode *node) {
    if (node == NULL) {
        return;
    }
    while (node->prev != NULL) {
        node = node->prev;
    }
    free_command_list(node);
}

CommandNode *create_command_list_head() {
    CommandNode *head = (CommandNode *)calloc(1, sizeof(CommandNode));
    if (head == NULL){
        return head;
    }
    head->is_head = true;
    return head;
}
